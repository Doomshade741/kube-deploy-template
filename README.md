## docker
Příklad Dockerfile pro build image.

## kustomization
Přiklad deklarativního deploymentu do kubernetes.

## .gitlab-ci
Příklady použití Gitlab CI/CD.
* Build docker kontejneru v Gitlabu
* Deployment do kubernetes
